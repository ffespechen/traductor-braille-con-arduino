const int primerPin = 4;
const int cantidadLeds = 6;
const int pausa = 500;


void setup() {

  for(int i=primerPin; i<=(primerPin+cantidadLeds); i++)
  {
    pinMode(i, OUTPUT);
  }

}


void borrarLeds()
{
  int borrar[6] = {LOW, LOW, LOW, LOW, LOW, LOW};
  
  for(int j=0; j<cantidadLeds; j++)
  {
    digitalWrite(primerPin+j, borrar[j]);
  }
  
  delay(pausa);
}

void loop() {

int numeros[10][6] = {{LOW, HIGH, HIGH, HIGH, LOW, LOW},
                      {HIGH, LOW, LOW, LOW, LOW, LOW},
                      {HIGH, LOW, HIGH, LOW, LOW, LOW},
                      {HIGH, HIGH, LOW, LOW, LOW, LOW},
                      {HIGH, HIGH, LOW, HIGH, LOW, LOW},
                      {HIGH, LOW, LOW, HIGH, LOW, LOW},
                      {HIGH, HIGH, HIGH, LOW, LOW, LOW},
                      {HIGH, HIGH, HIGH, HIGH, LOW, LOW},
                      {HIGH, LOW, HIGH, HIGH, LOW, LOW},
                      {LOW, HIGH, HIGH, LOW, LOW, LOW}};

for(int k=0; k<10; k++)
{
  for(int indice=0; indice<cantidadLeds; indice++)
  {
    digitalWrite(indice+primerPin, numeros[k][indice]);
    
  }

  delay(2000);

  borrarLeds();
}

}
