const int primerPin = 4;
const int cantidadLeds = 6;
const int pausa = 500;

int a[] = {HIGH, LOW, LOW, LOW, LOW, LOW};
int b[] = {HIGH, LOW, HIGH, LOW, LOW, LOW};
int c[] = {HIGH, HIGH, LOW, LOW, LOW, LOW};
int d[] = {HIGH, HIGH, LOW, HIGH, LOW, LOW};
int e[] = {HIGH, LOW, LOW, HIGH, LOW, LOW};
int f[] = {HIGH, HIGH, HIGH, LOW, LOW, LOW};
int g[] = {HIGH, HIGH, HIGH, HIGH, LOW, LOW};
int h[] = {HIGH, LOW, HIGH, HIGH, LOW, LOW};
int i[] = {LOW, HIGH, HIGH, LOW, LOW, LOW};
int j[] = {LOW, HIGH, HIGH, HIGH, LOW, LOW};
int k[] = {HIGH, LOW, LOW, LOW, HIGH, LOW};
int l[] = {HIGH, LOW, HIGH, LOW, HIGH, LOW};
int m[] = {HIGH, HIGH, LOW, LOW, HIGH, LOW};
int n[] = {HIGH, HIGH, LOW, HIGH, HIGH, LOW};
int o[] = {HIGH, LOW, LOW, HIGH, HIGH, LOW};
int p[] = {HIGH, HIGH, HIGH, LOW, HIGH, LOW};
int q[] = {HIGH, HIGH, HIGH, HIGH, HIGH, LOW};
int r[] = {HIGH, LOW, HIGH, HIGH, HIGH, LOW};
int s[] = {LOW, HIGH, HIGH, LOW, HIGH, LOW};
int t[] = {LOW, HIGH, HIGH, HIGH, HIGH, LOW};
int u[] = {HIGH, LOW, LOW, LOW, HIGH, HIGH};
int v[] = {HIGH, LOW, HIGH, LOW, HIGH, HIGH};
int w[] = {LOW, HIGH, HIGH, HIGH, LOW, HIGH};
int x[] = {HIGH, HIGH, LOW, LOW, HIGH, HIGH};
int y[] = {HIGH, HIGH, LOW, HIGH, HIGH, HIGH};
int z[] = {HIGH, LOW, LOW, HIGH, HIGH, HIGH};

void setup() {

  for(int i=primerPin; i<=(primerPin+cantidadLeds); i++)
  {
    pinMode(i, OUTPUT);
  }

  Serial.begin(9600);

}


void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    Serial.println(inChar);
    // add it to the inputString:

    switch(tolower(inChar))
    {
      case 'a':
        traducirCaracteres(a);
        break;

      case 'b':
        traducirCaracteres(b);
        break;

      case 'c':
        traducirCaracteres(c);
        break;

      case 'd':
        traducirCaracteres(d);
        break;

      case 'e':
        traducirCaracteres(e);
        break;

      case 'f':
        traducirCaracteres(f);
        break;

      case 'g':
        traducirCaracteres(g);
        break;
        
      case 'h':
        traducirCaracteres(h);
        break;

      case 'i':
        traducirCaracteres(i);
        break;

      case 'j':
        traducirCaracteres(j);
        break;

      case 'k':
        traducirCaracteres(k);
        break;

      case 'l':
        traducirCaracteres(l);
        break;

      case 'm':
        traducirCaracteres(m);
        break;

      case 'n':
        traducirCaracteres(n);
        break;

      case 'o':
        traducirCaracteres(o);
        break;

      case 'p':
        traducirCaracteres(p);
        break;

      case 'q':
        traducirCaracteres(q);
        break;

      case 'r':
        traducirCaracteres(r);
        break;

      case 's':
        traducirCaracteres(s);
        break;

      case 't':
        traducirCaracteres(t);
        break;

      case 'u':
        traducirCaracteres(u);
        break;

      case 'v':
        traducirCaracteres(v);
        break;

      case 'w':
        traducirCaracteres(w);
        break;

      case 'x':
        traducirCaracteres(x);
        break;

      case 'y':
        traducirCaracteres(y);
        break;

      case 'z':
        traducirCaracteres(z);
        break;
    }


     apagarLeds();
  }
}


void traducirCaracteres(int caracter[])
{
  for(int j=0; j<cantidadLeds; j++)
  {
    digitalWrite(primerPin+j, caracter[j]);
  }
  
  delay(1000);
  
}

void apagarLeds()
{
  int borrar[6] = {LOW, LOW, LOW, LOW, LOW, LOW};
  
  for(int j=0; j<cantidadLeds; j++)
  {
    digitalWrite(primerPin+j, borrar[j]);
  }
  
  delay(pausa);
}



void loop() {



}
