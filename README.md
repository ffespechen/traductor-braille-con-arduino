# Traductor Braille con Arduino

El proyecto lee desde el puerto serie una secuencia de caracteres y los convierte a señales HIGH, LOW en la disposición de los caracteres del alfabeto Braille.

Si bien, y a título ilustrativo se muestra con LEDS, las señales que reciben los pines (4 al 9) pueden ser aprovechadas por cualquier otro actuador.

Un programa alternativo traduce los números.

Como estas aplicaciones persiguen fundamentalmente un fin didáctico, se utilizaron matrices y estructuras switch para identificar los caracteres, en detrimento de otros procedimientos que pudieran ser más eficientes, como enmascaramiento utilizando valores binarios para almacenar los estados de los pines en cada caracter Braille